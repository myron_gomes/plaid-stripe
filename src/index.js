(() => {
  const notify = (text) => {
    const errorDiv = document.body.querySelector('#notification');
    if (errorDiv) {
      console.log(text);
      errorDiv.innerHTML = text;
    }
  };

  const onLoad = () => notify('Verifying');
  const onSuccess = (data) => notify(`Account successfully verified`); // ${data.stripe_bank_account_token}
  const onError = (error) => notify(`Error: ${error.error_message}`);
  const onExit = () => notify('Closed without verifying');

  //notify('Plaid Link');
  document.getElementById('linkButton').onclick = () => createPlaidLink(onLoad, onSuccess, onError, onExit).open();
})();