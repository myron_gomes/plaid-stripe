'use strict';

const cfg = {
  selectAccount: true,
  env: 'development',
  clientName: 'Client Name',
  serverUrl: 'https://plaid.spoonity.com:3001',
  public_key: 'ebb9c8f9e9177340ca935629e979d9',
};

function createPlaidLink(onLoad, onSuccess, onError, onExit) { // Callbacks my be 'null', if not needed
  const onAuthenticated = async (public_token, metadata) => {
    const params = `?public_token=${public_token}&account_id=${metadata.account_id}`;
    const rawData = await fetch(cfg.serverUrl + '/create_bank_account_token' + params, { method: 'GET' });

    const data = await rawData.json();
    if (data.error_code) {
      onError(data);
    } else {
      onSuccess(data);
    }
  };

  return Plaid.create({
    selectAccount: cfg.selectAccount,
    env: cfg.env,
    clientName: cfg.clientName,
    key: cfg.public_key,
    product: ['auth'],
    onLoad,
    onSuccess: onAuthenticated,
    onExit,
  });
}