const request = require('request');
const express = require('express');
const bodyParser = require("body-parser");
const path = require('path');
const cors = require('cors');
const app = express();
const fs = require('fs');
const https = require('https');

const cfg = {
  port: 3001,
  env: 'development',
  client_id: '5afdd0669c2fc3001231fc56',
  secret: '3ad31cc43c5afac483d3fa38b957e3',
};

//katmai ssl start
const key = fs.readFileSync('/etc/httpd/ssl/wildcard_spoonity_2017.key');
const cert = fs.readFileSync( '/etc/httpd/ssl/wildcard_spoonity_2017.crt' );
const ca = fs.readFileSync( '/etc/httpd/ssl/wildcard_spoonity_2017_comodo_bundle.crt' );

const options = {
  key: key,
  cert: cert,
  ca: ca
};

//katmai ssl end

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/src', express.static('src'));

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/create_bank_account_token', async function(req, res) {
  const public_token = req.query.public_token;
  const account_id = req.query.account_id;
  console.log(
    'Got GET request',
    `public_token = ${public_token}`,
    `account_id = ${account_id}`
  );

  const handleBankAccountToken = (error, response, body) => { // Fourth step (last)
    console.log('Fourth step have been called (handleBankAccountToken)', body);
    if (error) {
      res.send(error);
    } else {
      console.log('Sent success response');
      res.send(body); // Return object with token
    }
  };

  const createBankAccountToken = (access_token) => { // Third step
    console.log('Third step have been called (createBankAccountToken)', `access_token = ${access_token}`);
    const data = {
      client_id: cfg.client_id,
      secret: cfg.secret,
      access_token: access_token,
      account_id: account_id,
    };

    request.post(
      `https://${cfg.env}.plaid.com/processor/stripe/bank_account_token/create`,
      {json: data},
      handleBankAccountToken, // Go to fourth step
    );
  };

  const handleAccessToken = (error, response, body) => { // Seconds step
    console.log('Second step have been called (handleAccessToken)', body);
    if (error) {
      res.send(error);
    } else {
      createBankAccountToken(body.access_token); // Go to third step
    }
  };

  const exchangePublicTokenOnAccessToken = () => { // First step
    console.log('First step have been called (exchangePublicTokenOnAccessToken)');
    const data = {
      client_id: cfg.client_id,
      secret: cfg.secret,
      public_token: public_token,
    };

    request.post(
      `https://${cfg.env}.plaid.com/item/public_token/exchange`,
      {json: data},
      handleAccessToken, // Go to second step
    );
  };

  exchangePublicTokenOnAccessToken(); // Go to frist step
});

const serverHttps = https.createServer(options, app);
serverHttps.listen(cfg.port, () => {
  console.log('Https listening on port ' + cfg.port);
});